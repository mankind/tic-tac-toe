module Ttt
  class Board
   
    attr_reader :grid

    def initialize(input_array_hash = {})
      @grid = input_array_hash.fetch(:grid, board_default_grid)
    end

    def fetch_cell(x, y)
      grid[y][x]
    end

    def set_cell(x, y, value)
      fetch_cell(x, y).value = value
    end

    def game_finished
      return :winner if winner?
      return :draw if draw?
      false
    end


    private

    def board_default_grid
      Array.new(3) { Array.new(3) { Cell.new } }
    end

  end
end
