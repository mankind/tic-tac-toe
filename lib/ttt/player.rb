module Ttt
  class Player

    attr_reader :color, :name

    def initialize(player_hash)
      @color = player_hash.fetch(:color)
      @name = player_hash.fetch(:name)
    end
  end
end
