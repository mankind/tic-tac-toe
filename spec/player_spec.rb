require "spec_helper"

module Ttt

  describe Player do

    context "#initialize" do
      it "raises an exception  when initialized with empty hash" do
        expect {Player.new({})}.to raise_exception
      end

      it "does not raise an error when initialized with a valid hash" do
        player_hash = { color: "red", name: "edward" }
        expect { Player.new(player_hash) }.to_not raise_exception
     end

    end

    context "#color" do
      it "returns the color" do
        player_hash = { color: "red", name: "edward" }
        player = Player.new(player_hash)
        expect(player.color).to eq "red"
      end
    end
 
   context "#name" do
     it "returns the player's name" do
       player_hash = { color: "red", name: "edward" }
       player = Player.new(player_hash)
       expect(player.name).to eq "edward"
    end
  end

  end

end

