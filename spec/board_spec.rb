require 'spec_helper'

module Ttt

  describe Board do
    context "#initialize" do
      it "initializes the board with a grid" do
        expect { Board.new(grid: "grid") }.to_not raise_error
      end

      it "default to setting the grid with 3 rows" do
        board = Board.new
        expect(board.grid.size).to eq(3)
     end
 
     it "defaults to creating 3 things in each row" do
       board = Board.new
       board.grid.each do |row|
         expect(row.size).to eq(3)
       end
     end

    end

    context "#grid" do
      it "returns the grid" do
        board = Board.new(grid: "the_grid")
          expect(board.grid).to eq "the_grid"
        end
    end

    context "#fetch_cell" do
      it "returns the cell based on the (x, y) coordinate" do
        grid = [["", "", ""], ["", "", "stuff"], ["", "", ""]]
        board = Board.new(grid: grid)
        expect(board.fetch_cell(2, 1)).to eq "stuff"
      end
    end

    context "#set_cell" do
      it "updates the value of the cell object at a (x, y) coordinate" do
        Edward = Struct.new(:value)
        grid = [[Edward.new("going_big_time"), "", ""], ["", "", ""], ["", "", ""]]
        board = Board.new(grid: grid)
        board.set_cell(0, 0, "yeah")
        expect(board.fetch_cell(0, 0).value).to eq "yeah"
      end
    end    

    context "#game_finished" do
      it "returns :winner if winner? is true" do
        board = Board.new
        allow(board).to receive(:winner?) {true}
        expect(board.game_finished).to eq :winner
      end
 
      it "returns :draw if winner? is false and draw? is true" do
        board = Board.new
        allow(board).to receive(:winner?) {false}
        allow(board).to receive(:draw?) {true}
        expect(board.game_finished).to eq :draw
      end
 
      it "returns false if winner? is false and draw? is false" do
        board = Board.new
        allow(board).to receive(:winner?) {false}
        allow(board).to receive(:draw?) {false}
        expect(board.game_finished).to be false
      end
    end

  end

end
